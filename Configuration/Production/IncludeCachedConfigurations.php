<?php
if (FLOW_PATH_ROOT !== '/var/www/neos/' || !file_exists('/var/www/neos/Data/Temporary/Production/Configuration/ProductionConfigurations.php')) {
	unlink(__FILE__);
	return array();
}
return require '/var/www/neos/Data/Temporary/Production/Configuration/ProductionConfigurations.php';