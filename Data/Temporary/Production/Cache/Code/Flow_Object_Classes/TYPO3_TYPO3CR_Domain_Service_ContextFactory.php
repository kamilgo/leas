<?php
namespace TYPO3\TYPO3CR\Domain\Service;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3CR".               *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU General Public License, either version 3 of the   *
 * License, or (at your option) any later version.                        *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

/**
 * The ContextFactory makes sure you don't create context instances with
 * the same properties twice. Calling create() with the same parameters
 * a second time will return the _same_ Context instance again.
 * Refer to 'ContextFactoryInterface' instead of 'ContextFactory' when
 * injecting this factory into your own class.
 *
 * @Flow\Scope("singleton")
 */
class ContextFactory_Original implements ContextFactoryInterface {

	/**
	 * @var array<\TYPO3\TYPO3CR\Domain\Service\ContextInterface>
	 */
	protected $contextInstances = array();

	/**
	 * The context implementation this factory will create
	 *
	 * @var string
	 */
	protected $contextImplementation = 'TYPO3\TYPO3CR\Domain\Service\Context';

	/**
	 * Create the context from the given properties. If a context with those properties was already
	 * created before then the existing one is returned.
	 *
	 * The context properties to give depend on the implementation of the context object, for the
	 * TYPO3\TYPO3CR\Domain\Service\Context it should look like this:
	 *
	 * array(
	 * 		'workspaceName' => 'live',
	 * 		'currentDateTime' => new \TYPO3\Flow\Utility\Now(),
	 * 		'locale' => new \TYPO3\Flow\I18n\Locale('mul_ZZ'),
	 * 		'invisibleContentShown' => FALSE,
	 * 		'removedContentShown' => FALSE,
	 * 		'inaccessibleContentShown' => FALSE
	 * )
	 *
	 * This array also shows the defaults that get used if you don't provide a certain property.
	 *
	 * @param array $contextProperties
	 * @return \TYPO3\TYPO3CR\Domain\Service\ContextInterface
	 * @api
	 */
	public function create(array $contextProperties) {
		$contextIdentifier = $this->getIdentifier($contextProperties);
		if (!isset($this->contextInstances[$contextIdentifier])) {
			$contextProperties = $this->mergeContextPropertiesWithDefaults($contextProperties);
			$this->validateContextProperties($contextProperties);
			$context = $this->buildContextInstance($contextProperties);
			$this->contextInstances[$contextIdentifier] = $context;
		}

		return $this->contextInstances[$contextIdentifier];
	}

	/**
	 * Creates the actual Context instance.
	 * This needs to be overriden if the Builder is extended.
	 *
	 * @param array $contextProperties
	 * @return \TYPO3\TYPO3CR\Domain\Service\Context
	 */
	protected function buildContextInstance(array $contextProperties) {
		return new \TYPO3\TYPO3CR\Domain\Service\Context($contextProperties['workspaceName'], $contextProperties['currentDateTime'], $contextProperties['locale'], $contextProperties['invisibleContentShown'], $contextProperties['removedContentShown'], $contextProperties['inaccessibleContentShown']);
	}

	/**
	 * Merges the given context properties with sane defaults for the context implementation.
	 *
	 * @param array $contextProperties
	 * @return array
	 */
	protected function mergeContextPropertiesWithDefaults(array $contextProperties) {
		$defaultContextProperties = array (
			'workspaceName' => 'live',
			'currentDateTime' => new \TYPO3\Flow\Utility\Now(),
			'locale' => new \TYPO3\Flow\I18n\Locale('mul_ZZ'),
			'invisibleContentShown' => FALSE,
			'removedContentShown' => FALSE,
			'inaccessibleContentShown' => FALSE
		);

		return \TYPO3\Flow\Utility\Arrays::arrayMergeRecursiveOverrule($defaultContextProperties, $contextProperties, TRUE);
	}

	/**
	 * Provides a way to identify a context to prevent duplicate context objects.
	 *
	 * @param array $contextProperties
	 * @return string
	 */
	protected function getIdentifier(array $contextProperties) {
		return md5($this->getIdentifierSource($contextProperties));
	}

	/**
	 * This creates the actual identifier and needs to be overriden by builders extending this.
	 *
	 * @param array $contextProperties
	 * @return string
	 */
	protected function getIdentifierSource(array $contextProperties) {
		ksort($contextProperties);
		$identifierSource = $this->contextImplementation;
		foreach ($contextProperties as $propertyValue) {
			$stringValue = $propertyValue instanceof \DateTime ? $propertyValue->getTimestamp() : (string)$propertyValue;
			$identifierSource .= ':' . $stringValue;
		}

		return $identifierSource;
	}

	/**
	 * @param array $contextProperties
	 * @return void
	 * @throws \TYPO3\TYPO3CR\Exception\InvalidNodeContextException
	 */
	protected function validateContextProperties($contextProperties) {
		if (isset($contextProperties['workspaceName'])) {
			if (!is_string($contextProperties['workspaceName']) || $contextProperties['workspaceName'] === '') {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set a workspaceName in the context that was either no string or an empty string.', 1373144966);
			}
		}
		if (isset($contextProperties['invisibleContentShown'])) {
			if (!is_bool($contextProperties['invisibleContentShown'])) {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set invisibleContentShown in the context and did not provide a boolean value.', 1373145239);
			}
		}
		if (isset($contextProperties['removedContentShown'])) {
			if (!is_bool($contextProperties['removedContentShown'])) {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set removedContentShown in the context and did not provide a boolean value.', 1373145239);
			}
		}
		if (isset($contextProperties['inaccessibleContentShown'])) {
			if (!is_bool($contextProperties['inaccessibleContentShown'])) {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set inaccessibleContentShown in the context and did not provide a boolean value.', 1373145239);
			}
		}
		if (isset($contextProperties['currentDateTime'])) {
			if (!$contextProperties['currentDateTime'] instanceof \DateTime) {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set currentDateTime in the context and did not provide a DateTime object as value.', 1373145297);
			}
		}
		if (isset($contextProperties['locale'])) {
			if (!$contextProperties['locale'] instanceof \TYPO3\Flow\I18n\Locale) {
				throw new \TYPO3\TYPO3CR\Exception\InvalidNodeContextException('You tried to set locale in the context and did not provide a \\TYPO3\\Flow\\I18n\\Locale object as value.', 1373145384);
			}
		}
	}
}
namespace TYPO3\TYPO3CR\Domain\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The ContextFactory makes sure you don't create context instances with
 * the same properties twice. Calling create() with the same parameters
 * a second time will return the _same_ Context instance again.
 * Refer to 'ContextFactoryInterface' instead of 'ContextFactory' when
 * injecting this factory into your own class.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ContextFactory extends ContextFactory_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {


	/**
	 * Autogenerated Proxy Method
	 */
	public function __construct() {
		if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Service\ContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\ContextFactory', $this);
	}

	/**
	 * Autogenerated Proxy Method
	 */
	 public function __wakeup() {
		if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Service\ContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\ContextFactory', $this);

	if (property_exists($this, 'Flow_Persistence_RelatedEntities') && is_array($this->Flow_Persistence_RelatedEntities)) {
		$persistenceManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface');
		foreach ($this->Flow_Persistence_RelatedEntities as $entityInformation) {
			$entity = $persistenceManager->getObjectByIdentifier($entityInformation['identifier'], $entityInformation['entityType'], TRUE);
			if (isset($entityInformation['entityPath'])) {
				$this->$entityInformation['propertyName'] = \TYPO3\Flow\Utility\Arrays::setValueByPath($this->$entityInformation['propertyName'], $entityInformation['entityPath'], $entity);
			} else {
				$this->$entityInformation['propertyName'] = $entity;
			}
		}
		unset($this->Flow_Persistence_RelatedEntities);
	}
			}

	/**
	 * Autogenerated Proxy Method
	 */
	 public function __sleep() {
		$result = NULL;
		$this->Flow_Object_PropertiesToSerialize = array();
	$reflectionService = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService');
	$reflectedClass = new \ReflectionClass('TYPO3\TYPO3CR\Domain\Service\ContextFactory');
	$allReflectedProperties = $reflectedClass->getProperties();
	foreach ($allReflectedProperties as $reflectionProperty) {
		$propertyName = $reflectionProperty->name;
		if (in_array($propertyName, array('Flow_Aop_Proxy_targetMethodsAndGroupedAdvices', 'Flow_Aop_Proxy_groupedAdviceChains', 'Flow_Aop_Proxy_methodIsInAdviceMode'))) continue;
		if ($reflectionService->isPropertyAnnotatedWith('TYPO3\TYPO3CR\Domain\Service\ContextFactory', $propertyName, 'TYPO3\Flow\Annotations\Transient')) continue;
		if (is_array($this->$propertyName) || (is_object($this->$propertyName) && ($this->$propertyName instanceof \ArrayObject || $this->$propertyName instanceof \SplObjectStorage ||$this->$propertyName instanceof \Doctrine\Common\Collections\Collection))) {
			foreach ($this->$propertyName as $key => $value) {
				$this->searchForEntitiesAndStoreIdentifierArray((string)$key, $value, $propertyName);
			}
		}
		if (is_object($this->$propertyName) && !$this->$propertyName instanceof \Doctrine\Common\Collections\Collection) {
			if ($this->$propertyName instanceof \Doctrine\ORM\Proxy\Proxy) {
				$className = get_parent_class($this->$propertyName);
			} else {
				$className = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->getObjectNameByClassName(get_class($this->$propertyName));
			}
			if ($this->$propertyName instanceof \TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface && !\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface')->isNewObject($this->$propertyName) || $this->$propertyName instanceof \Doctrine\ORM\Proxy\Proxy) {
				if (!property_exists($this, 'Flow_Persistence_RelatedEntities') || !is_array($this->Flow_Persistence_RelatedEntities)) {
					$this->Flow_Persistence_RelatedEntities = array();
					$this->Flow_Object_PropertiesToSerialize[] = 'Flow_Persistence_RelatedEntities';
				}
				$identifier = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface')->getIdentifierByObject($this->$propertyName);
				if (!$identifier && $this->$propertyName instanceof \Doctrine\ORM\Proxy\Proxy) {
					$identifier = current(\TYPO3\Flow\Reflection\ObjectAccess::getProperty($this->$propertyName, '_identifier', TRUE));
				}
				$this->Flow_Persistence_RelatedEntities[$propertyName] = array(
					'propertyName' => $propertyName,
					'entityType' => $className,
					'identifier' => $identifier
				);
				continue;
			}
			if ($className !== FALSE && (\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->getScope($className) === \TYPO3\Flow\Object\Configuration\Configuration::SCOPE_SINGLETON || $className === 'TYPO3\Flow\Object\DependencyInjection\DependencyProxy')) {
				continue;
			}
		}
		$this->Flow_Object_PropertiesToSerialize[] = $propertyName;
	}
	$result = $this->Flow_Object_PropertiesToSerialize;
		return $result;
	}

	/**
	 * Autogenerated Proxy Method
	 */
	 private function searchForEntitiesAndStoreIdentifierArray($path, $propertyValue, $originalPropertyName) {

		if (is_array($propertyValue) || (is_object($propertyValue) && ($propertyValue instanceof \ArrayObject || $propertyValue instanceof \SplObjectStorage))) {
			foreach ($propertyValue as $key => $value) {
				$this->searchForEntitiesAndStoreIdentifierArray($path . '.' . $key, $value, $originalPropertyName);
			}
		} elseif ($propertyValue instanceof \TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface && !\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface')->isNewObject($propertyValue) || $propertyValue instanceof \Doctrine\ORM\Proxy\Proxy) {
			if (!property_exists($this, 'Flow_Persistence_RelatedEntities') || !is_array($this->Flow_Persistence_RelatedEntities)) {
				$this->Flow_Persistence_RelatedEntities = array();
				$this->Flow_Object_PropertiesToSerialize[] = 'Flow_Persistence_RelatedEntities';
			}
			if ($propertyValue instanceof \Doctrine\ORM\Proxy\Proxy) {
				$className = get_parent_class($propertyValue);
			} else {
				$className = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->getObjectNameByClassName(get_class($propertyValue));
			}
			$identifier = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface')->getIdentifierByObject($propertyValue);
			if (!$identifier && $propertyValue instanceof \Doctrine\ORM\Proxy\Proxy) {
				$identifier = current(\TYPO3\Flow\Reflection\ObjectAccess::getProperty($propertyValue, '_identifier', TRUE));
			}
			$this->Flow_Persistence_RelatedEntities[$originalPropertyName . '.' . $path] = array(
				'propertyName' => $originalPropertyName,
				'entityType' => $className,
				'identifier' => $identifier,
				'entityPath' => $path
			);
			$this->$originalPropertyName = \TYPO3\Flow\Utility\Arrays::setValueByPath($this->$originalPropertyName, $path, NULL);
		}
			}
}
#