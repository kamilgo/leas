<?php
class FluidCache_TYPO3_Neos_Login_action_index_0224e44d8cad4f485f3ebd379dbabe02b0408c3d extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return 'Default';
}
public function hasLayout() {
return TRUE;
}

/**
 * section head
 */
public function section_1a954628a960aaef81d7b2d4521929579f3541e6(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<title>TYPO3 Neos Login</title>
	<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1 = array();
$arguments1['path'] = 'Styles/Login.css';
$arguments1['package'] = NULL;
$arguments1['resource'] = NULL;
$arguments1['localize'] = true;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper3 = $self->getViewHelper('$viewHelper3', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper3->setArguments($arguments1);
$viewHelper3->setRenderingContext($renderingContext);
$viewHelper3->setRenderChildrenClosure($renderChildrenClosure2);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output0 .= $viewHelper3->initializeArgumentsAndRender();

$output0 .= '" />
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments4 = array();
$arguments4['path'] = 'Library/jquery/jquery-1.10.2.js';
$arguments4['package'] = NULL;
$arguments4['resource'] = NULL;
$arguments4['localize'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper6 = $self->getViewHelper('$viewHelper6', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper6->setArguments($arguments4);
$viewHelper6->setRenderingContext($renderingContext);
$viewHelper6->setRenderChildrenClosure($renderChildrenClosure5);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output0 .= $viewHelper6->initializeArgumentsAndRender();

$output0 .= '"></script>
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments7 = array();
$arguments7['path'] = 'Library/jquery-ui/js/jquery-ui-1.10.3.custom.js';
$arguments7['package'] = NULL;
$arguments7['resource'] = NULL;
$arguments7['localize'] = true;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper9 = $self->getViewHelper('$viewHelper9', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper9->setArguments($arguments7);
$viewHelper9->setRenderingContext($renderingContext);
$viewHelper9->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output0 .= $viewHelper9->initializeArgumentsAndRender();

$output0 .= '"></script>
';

return $output0;
}
/**
 * section body
 */
public function section_02083f4579e08a612425c0c1a17ee47add783b94(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output10 = '';

$output10 .= '
	<body class="neos">
		<div id="neos-login-box">
			<div class="neos-login-logo">
				<img src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments11 = array();
$arguments11['path'] = 'Images/Login/ApplicationLogo.png';
$arguments11['package'] = NULL;
$arguments11['resource'] = NULL;
$arguments11['localize'] = true;
$renderChildrenClosure12 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper13 = $self->getViewHelper('$viewHelper13', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper13->setArguments($arguments11);
$viewHelper13->setRenderingContext($renderingContext);
$viewHelper13->setRenderChildrenClosure($renderChildrenClosure12);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output10 .= $viewHelper13->initializeArgumentsAndRender();

$output10 .= '" alt="TYPO3 Neos" />
			</div>
			<div class="neos-login-body neos">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments14 = array();
$arguments14['name'] = 'login';
$arguments14['action'] = 'authenticate';
$arguments14['additionalAttributes'] = NULL;
$arguments14['arguments'] = array (
);
$arguments14['controller'] = NULL;
$arguments14['package'] = NULL;
$arguments14['subpackage'] = NULL;
$arguments14['object'] = NULL;
$arguments14['section'] = '';
$arguments14['format'] = '';
$arguments14['additionalParams'] = array (
);
$arguments14['absolute'] = false;
$arguments14['addQueryString'] = false;
$arguments14['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments14['fieldNamePrefix'] = NULL;
$arguments14['actionUri'] = NULL;
$arguments14['objectName'] = NULL;
$arguments14['useParentRequest'] = false;
$arguments14['enctype'] = NULL;
$arguments14['method'] = NULL;
$arguments14['onreset'] = NULL;
$arguments14['onsubmit'] = NULL;
$arguments14['class'] = NULL;
$arguments14['dir'] = NULL;
$arguments14['id'] = NULL;
$arguments14['lang'] = NULL;
$arguments14['style'] = NULL;
$arguments14['title'] = NULL;
$arguments14['accesskey'] = NULL;
$arguments14['tabindex'] = NULL;
$arguments14['onclick'] = NULL;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
$output16 = '';

$output16 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments17 = array();
$arguments17['name'] = 'lastVisitedNode';
$arguments17['additionalAttributes'] = NULL;
$arguments17['value'] = NULL;
$arguments17['property'] = NULL;
$arguments17['class'] = NULL;
$arguments17['dir'] = NULL;
$arguments17['id'] = NULL;
$arguments17['lang'] = NULL;
$arguments17['style'] = NULL;
$arguments17['title'] = NULL;
$arguments17['accesskey'] = NULL;
$arguments17['tabindex'] = NULL;
$arguments17['onclick'] = NULL;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper19 = $self->getViewHelper('$viewHelper19', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper19->setArguments($arguments17);
$viewHelper19->setRenderingContext($renderingContext);
$viewHelper19->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output16 .= $viewHelper19->initializeArgumentsAndRender();

$output16 .= '
					<fieldset>
						<div class="neos-controls">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments20 = array();
$arguments20['id'] = 'username';
$arguments20['type'] = 'text';
$arguments20['placeholder'] = 'Username';
$arguments20['class'] = 'neos-span12';
$arguments20['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array21 = array();
$array21['autofocus'] = 'autofocus';
$arguments20['additionalAttributes'] = $array21;
$arguments20['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments20['required'] = NULL;
$arguments20['property'] = NULL;
$arguments20['disabled'] = NULL;
$arguments20['maxlength'] = NULL;
$arguments20['readonly'] = NULL;
$arguments20['size'] = NULL;
$arguments20['autofocus'] = NULL;
$arguments20['errorClass'] = 'f3-form-error';
$arguments20['dir'] = NULL;
$arguments20['lang'] = NULL;
$arguments20['style'] = NULL;
$arguments20['title'] = NULL;
$arguments20['accesskey'] = NULL;
$arguments20['tabindex'] = NULL;
$arguments20['onclick'] = NULL;
$renderChildrenClosure22 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper23 = $self->getViewHelper('$viewHelper23', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper23->setArguments($arguments20);
$viewHelper23->setRenderingContext($renderingContext);
$viewHelper23->setRenderChildrenClosure($renderChildrenClosure22);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output16 .= $viewHelper23->initializeArgumentsAndRender();

$output16 .= '
						</div>
						<div class="neos-controls">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments24 = array();
$arguments24['id'] = 'password';
$arguments24['type'] = 'password';
$arguments24['placeholder'] = 'Password';
$arguments24['class'] = 'neos-span12';
$arguments24['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments24['additionalAttributes'] = NULL;
$arguments24['required'] = NULL;
$arguments24['value'] = NULL;
$arguments24['property'] = NULL;
$arguments24['disabled'] = NULL;
$arguments24['maxlength'] = NULL;
$arguments24['readonly'] = NULL;
$arguments24['size'] = NULL;
$arguments24['autofocus'] = NULL;
$arguments24['errorClass'] = 'f3-form-error';
$arguments24['dir'] = NULL;
$arguments24['lang'] = NULL;
$arguments24['style'] = NULL;
$arguments24['title'] = NULL;
$arguments24['accesskey'] = NULL;
$arguments24['tabindex'] = NULL;
$arguments24['onclick'] = NULL;
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper26 = $self->getViewHelper('$viewHelper26', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper26->setArguments($arguments24);
$viewHelper26->setRenderingContext($renderingContext);
$viewHelper26->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output16 .= $viewHelper26->initializeArgumentsAndRender();

$output16 .= '
						</div>
						<div class="neos-actions">
							<!-- Forgot password link will be here -->
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper
$arguments27 = array();
$arguments27['type'] = 'submit';
$arguments27['class'] = 'neos-span5 neos-pull-right neos-button neos-button-primary neos-button-warning neos-login-btn';
$arguments27['additionalAttributes'] = NULL;
$arguments27['name'] = NULL;
$arguments27['value'] = NULL;
$arguments27['property'] = NULL;
$arguments27['autofocus'] = NULL;
$arguments27['disabled'] = NULL;
$arguments27['form'] = NULL;
$arguments27['formaction'] = NULL;
$arguments27['formenctype'] = NULL;
$arguments27['formmethod'] = NULL;
$arguments27['formnovalidate'] = NULL;
$arguments27['formtarget'] = NULL;
$arguments27['dir'] = NULL;
$arguments27['id'] = NULL;
$arguments27['lang'] = NULL;
$arguments27['style'] = NULL;
$arguments27['title'] = NULL;
$arguments27['accesskey'] = NULL;
$arguments27['tabindex'] = NULL;
$arguments27['onclick'] = NULL;
$renderChildrenClosure28 = function() use ($renderingContext, $self) {
return '
								Login
							';
};
$viewHelper29 = $self->getViewHelper('$viewHelper29', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper');
$viewHelper29->setArguments($arguments27);
$viewHelper29->setRenderingContext($renderingContext);
$viewHelper29->setRenderChildrenClosure($renderChildrenClosure28);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper

$output16 .= $viewHelper29->initializeArgumentsAndRender();

$output16 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments30 = array();
$arguments30['as'] = 'flashMessages';
$arguments30['additionalAttributes'] = NULL;
$arguments30['severity'] = NULL;
$arguments30['class'] = NULL;
$arguments30['dir'] = NULL;
$arguments30['id'] = NULL;
$arguments30['lang'] = NULL;
$arguments30['style'] = NULL;
$arguments30['title'] = NULL;
$arguments30['accesskey'] = NULL;
$arguments30['tabindex'] = NULL;
$arguments30['onclick'] = NULL;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
$output32 = '';

$output32 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments33 = array();
$arguments33['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessages', $renderingContext);
$arguments33['as'] = 'flashMessage';
$arguments33['key'] = '';
$arguments33['reverse'] = false;
$arguments33['iteration'] = NULL;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
$output35 = '';

$output35 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments36 = array();
// Rendering Boolean node
// Rendering Array
$array37 = array();
$array37['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array38 = array();
$array38['0'] = 'OK';
$arguments36['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array37, $array38);
$arguments36['then'] = NULL;
$arguments36['else'] = NULL;
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-success">
									';
};
$viewHelper40 = $self->getViewHelper('$viewHelper40', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper40->setArguments($arguments36);
$viewHelper40->setRenderingContext($renderingContext);
$viewHelper40->setRenderChildrenClosure($renderChildrenClosure39);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper40->initializeArgumentsAndRender();

$output35 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments41 = array();
// Rendering Boolean node
// Rendering Array
$array42 = array();
$array42['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array43 = array();
$array43['0'] = 'Notice';
$arguments41['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array42, $array43);
$arguments41['then'] = NULL;
$arguments41['else'] = NULL;
$renderChildrenClosure44 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-notice">
									';
};
$viewHelper45 = $self->getViewHelper('$viewHelper45', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper45->setArguments($arguments41);
$viewHelper45->setRenderingContext($renderingContext);
$viewHelper45->setRenderChildrenClosure($renderChildrenClosure44);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper45->initializeArgumentsAndRender();

$output35 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments46 = array();
// Rendering Boolean node
// Rendering Array
$array47 = array();
$array47['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array48 = array();
$array48['0'] = 'Warning';
$arguments46['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array47, $array48);
$arguments46['then'] = NULL;
$arguments46['else'] = NULL;
$renderChildrenClosure49 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-warning">
									';
};
$viewHelper50 = $self->getViewHelper('$viewHelper50', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper50->setArguments($arguments46);
$viewHelper50->setRenderingContext($renderingContext);
$viewHelper50->setRenderChildrenClosure($renderChildrenClosure49);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper50->initializeArgumentsAndRender();

$output35 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments51 = array();
// Rendering Boolean node
// Rendering Array
$array52 = array();
$array52['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array53 = array();
$array53['0'] = 'Error';
$arguments51['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array52, $array53);
$arguments51['then'] = NULL;
$arguments51['else'] = NULL;
$renderChildrenClosure54 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-error">
									';
};
$viewHelper55 = $self->getViewHelper('$viewHelper55', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper55->setArguments($arguments51);
$viewHelper55->setRenderingContext($renderingContext);
$viewHelper55->setRenderChildrenClosure($renderChildrenClosure54);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper55->initializeArgumentsAndRender();

$output35 .= '
										<div class="neos-tooltip-arrow"></div>
										<div class="neos-tooltip-inner">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments56 = array();
$arguments56['value'] = NULL;
$renderChildrenClosure57 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.message', $renderingContext);
};
$viewHelper58 = $self->getViewHelper('$viewHelper58', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper58->setArguments($arguments56);
$viewHelper58->setRenderingContext($renderingContext);
$viewHelper58->setRenderChildrenClosure($renderChildrenClosure57);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output35 .= $viewHelper58->initializeArgumentsAndRender();

$output35 .= '</div>
									</div>
								';
return $output35;
};

$output32 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments33, $renderChildrenClosure34, $renderingContext);

$output32 .= '
							';
return $output32;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper59->setArguments($arguments30);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure31);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper

$output16 .= $viewHelper59->initializeArgumentsAndRender();

$output16 .= '
						</div>
					</fieldset>
				';
return $output16;
};
$viewHelper60 = $self->getViewHelper('$viewHelper60', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper60->setArguments($arguments14);
$viewHelper60->setRenderingContext($renderingContext);
$viewHelper60->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output10 .= $viewHelper60->initializeArgumentsAndRender();

$output10 .= '
			</div>
		</div>
		<div id="neos-login-footer">
			<p>
				<a href="http://neos.typo3.org" target="_blank">TYPO3 Neos</a> – © 2006-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments61 = array();
$arguments61['format'] = 'Y';
$arguments61['date'] = 'now';
$arguments61['forceLocale'] = NULL;
$arguments61['localeFormatType'] = NULL;
$arguments61['localeFormatLength'] = NULL;
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper63->setArguments($arguments61);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper

$output10 .= $viewHelper63->initializeArgumentsAndRender();

$output10 .= '
				This is free software, licensed under GPL3 or higher, and you are welcome to redistribute it under certain conditions; <a href="http://typo3.org/licenses" target="_blank">click for details.</a>
				TYPO3 Neos comes with ABSOLUTELY NO WARRANTY; <a href="http://typo3.org/licenses" target="_blank">click for details.</a>
				See <a href="http://neos.typo3.org" target="_blank">neos.typo3.org</a> for more details.
				Obstructing the appearance of this notice is prohibited by law.
			</p>
		</div>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments64 = array();
$arguments64['path'] = '2/js/bootstrap.min.js';
$arguments64['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments64['resource'] = NULL;
$arguments64['localize'] = true;
$renderChildrenClosure65 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper66 = $self->getViewHelper('$viewHelper66', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper66->setArguments($arguments64);
$viewHelper66->setRenderingContext($renderingContext);
$viewHelper66->setRenderChildrenClosure($renderChildrenClosure65);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output10 .= $viewHelper66->initializeArgumentsAndRender();

$output10 .= '"></script>
		<script>
			if ($(\'#username\').val()) {
				$(\'#password\').focus();
			}
			try {
				$(\'form[name="login"] input[name="lastVisitedNode"]\').val(sessionStorage.getItem(\'TYPO3.Neos.lastVisitedNode\'));
			} catch(e) {}
		</script>
	</body>
';

return $output10;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output67 = '';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments68 = array();
$arguments68['name'] = 'Default';
$renderChildrenClosure69 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper70 = $self->getViewHelper('$viewHelper70', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper70->setArguments($arguments68);
$viewHelper70->setRenderingContext($renderingContext);
$viewHelper70->setRenderChildrenClosure($renderChildrenClosure69);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper

$output67 .= $viewHelper70->initializeArgumentsAndRender();

$output67 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments71 = array();
$arguments71['name'] = 'head';
$renderChildrenClosure72 = function() use ($renderingContext, $self) {
$output73 = '';

$output73 .= '
	<title>TYPO3 Neos Login</title>
	<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments74 = array();
$arguments74['path'] = 'Styles/Login.css';
$arguments74['package'] = NULL;
$arguments74['resource'] = NULL;
$arguments74['localize'] = true;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper76 = $self->getViewHelper('$viewHelper76', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper76->setArguments($arguments74);
$viewHelper76->setRenderingContext($renderingContext);
$viewHelper76->setRenderChildrenClosure($renderChildrenClosure75);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output73 .= $viewHelper76->initializeArgumentsAndRender();

$output73 .= '" />
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments77 = array();
$arguments77['path'] = 'Library/jquery/jquery-1.10.2.js';
$arguments77['package'] = NULL;
$arguments77['resource'] = NULL;
$arguments77['localize'] = true;
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper79 = $self->getViewHelper('$viewHelper79', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper79->setArguments($arguments77);
$viewHelper79->setRenderingContext($renderingContext);
$viewHelper79->setRenderChildrenClosure($renderChildrenClosure78);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output73 .= $viewHelper79->initializeArgumentsAndRender();

$output73 .= '"></script>
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments80 = array();
$arguments80['path'] = 'Library/jquery-ui/js/jquery-ui-1.10.3.custom.js';
$arguments80['package'] = NULL;
$arguments80['resource'] = NULL;
$arguments80['localize'] = true;
$renderChildrenClosure81 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper82 = $self->getViewHelper('$viewHelper82', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper82->setArguments($arguments80);
$viewHelper82->setRenderingContext($renderingContext);
$viewHelper82->setRenderChildrenClosure($renderChildrenClosure81);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output73 .= $viewHelper82->initializeArgumentsAndRender();

$output73 .= '"></script>
';
return $output73;
};

$output67 .= '';

$output67 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments83 = array();
$arguments83['name'] = 'body';
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
$output85 = '';

$output85 .= '
	<body class="neos">
		<div id="neos-login-box">
			<div class="neos-login-logo">
				<img src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments86 = array();
$arguments86['path'] = 'Images/Login/ApplicationLogo.png';
$arguments86['package'] = NULL;
$arguments86['resource'] = NULL;
$arguments86['localize'] = true;
$renderChildrenClosure87 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper88 = $self->getViewHelper('$viewHelper88', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper88->setArguments($arguments86);
$viewHelper88->setRenderingContext($renderingContext);
$viewHelper88->setRenderChildrenClosure($renderChildrenClosure87);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output85 .= $viewHelper88->initializeArgumentsAndRender();

$output85 .= '" alt="TYPO3 Neos" />
			</div>
			<div class="neos-login-body neos">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments89 = array();
$arguments89['name'] = 'login';
$arguments89['action'] = 'authenticate';
$arguments89['additionalAttributes'] = NULL;
$arguments89['arguments'] = array (
);
$arguments89['controller'] = NULL;
$arguments89['package'] = NULL;
$arguments89['subpackage'] = NULL;
$arguments89['object'] = NULL;
$arguments89['section'] = '';
$arguments89['format'] = '';
$arguments89['additionalParams'] = array (
);
$arguments89['absolute'] = false;
$arguments89['addQueryString'] = false;
$arguments89['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments89['fieldNamePrefix'] = NULL;
$arguments89['actionUri'] = NULL;
$arguments89['objectName'] = NULL;
$arguments89['useParentRequest'] = false;
$arguments89['enctype'] = NULL;
$arguments89['method'] = NULL;
$arguments89['onreset'] = NULL;
$arguments89['onsubmit'] = NULL;
$arguments89['class'] = NULL;
$arguments89['dir'] = NULL;
$arguments89['id'] = NULL;
$arguments89['lang'] = NULL;
$arguments89['style'] = NULL;
$arguments89['title'] = NULL;
$arguments89['accesskey'] = NULL;
$arguments89['tabindex'] = NULL;
$arguments89['onclick'] = NULL;
$renderChildrenClosure90 = function() use ($renderingContext, $self) {
$output91 = '';

$output91 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments92 = array();
$arguments92['name'] = 'lastVisitedNode';
$arguments92['additionalAttributes'] = NULL;
$arguments92['value'] = NULL;
$arguments92['property'] = NULL;
$arguments92['class'] = NULL;
$arguments92['dir'] = NULL;
$arguments92['id'] = NULL;
$arguments92['lang'] = NULL;
$arguments92['style'] = NULL;
$arguments92['title'] = NULL;
$arguments92['accesskey'] = NULL;
$arguments92['tabindex'] = NULL;
$arguments92['onclick'] = NULL;
$renderChildrenClosure93 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper94 = $self->getViewHelper('$viewHelper94', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper94->setArguments($arguments92);
$viewHelper94->setRenderingContext($renderingContext);
$viewHelper94->setRenderChildrenClosure($renderChildrenClosure93);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output91 .= $viewHelper94->initializeArgumentsAndRender();

$output91 .= '
					<fieldset>
						<div class="neos-controls">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments95 = array();
$arguments95['id'] = 'username';
$arguments95['type'] = 'text';
$arguments95['placeholder'] = 'Username';
$arguments95['class'] = 'neos-span12';
$arguments95['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array96 = array();
$array96['autofocus'] = 'autofocus';
$arguments95['additionalAttributes'] = $array96;
$arguments95['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments95['required'] = NULL;
$arguments95['property'] = NULL;
$arguments95['disabled'] = NULL;
$arguments95['maxlength'] = NULL;
$arguments95['readonly'] = NULL;
$arguments95['size'] = NULL;
$arguments95['autofocus'] = NULL;
$arguments95['errorClass'] = 'f3-form-error';
$arguments95['dir'] = NULL;
$arguments95['lang'] = NULL;
$arguments95['style'] = NULL;
$arguments95['title'] = NULL;
$arguments95['accesskey'] = NULL;
$arguments95['tabindex'] = NULL;
$arguments95['onclick'] = NULL;
$renderChildrenClosure97 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper98 = $self->getViewHelper('$viewHelper98', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper98->setArguments($arguments95);
$viewHelper98->setRenderingContext($renderingContext);
$viewHelper98->setRenderChildrenClosure($renderChildrenClosure97);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output91 .= $viewHelper98->initializeArgumentsAndRender();

$output91 .= '
						</div>
						<div class="neos-controls">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments99 = array();
$arguments99['id'] = 'password';
$arguments99['type'] = 'password';
$arguments99['placeholder'] = 'Password';
$arguments99['class'] = 'neos-span12';
$arguments99['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments99['additionalAttributes'] = NULL;
$arguments99['required'] = NULL;
$arguments99['value'] = NULL;
$arguments99['property'] = NULL;
$arguments99['disabled'] = NULL;
$arguments99['maxlength'] = NULL;
$arguments99['readonly'] = NULL;
$arguments99['size'] = NULL;
$arguments99['autofocus'] = NULL;
$arguments99['errorClass'] = 'f3-form-error';
$arguments99['dir'] = NULL;
$arguments99['lang'] = NULL;
$arguments99['style'] = NULL;
$arguments99['title'] = NULL;
$arguments99['accesskey'] = NULL;
$arguments99['tabindex'] = NULL;
$arguments99['onclick'] = NULL;
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper101 = $self->getViewHelper('$viewHelper101', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper101->setArguments($arguments99);
$viewHelper101->setRenderingContext($renderingContext);
$viewHelper101->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output91 .= $viewHelper101->initializeArgumentsAndRender();

$output91 .= '
						</div>
						<div class="neos-actions">
							<!-- Forgot password link will be here -->
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper
$arguments102 = array();
$arguments102['type'] = 'submit';
$arguments102['class'] = 'neos-span5 neos-pull-right neos-button neos-button-primary neos-button-warning neos-login-btn';
$arguments102['additionalAttributes'] = NULL;
$arguments102['name'] = NULL;
$arguments102['value'] = NULL;
$arguments102['property'] = NULL;
$arguments102['autofocus'] = NULL;
$arguments102['disabled'] = NULL;
$arguments102['form'] = NULL;
$arguments102['formaction'] = NULL;
$arguments102['formenctype'] = NULL;
$arguments102['formmethod'] = NULL;
$arguments102['formnovalidate'] = NULL;
$arguments102['formtarget'] = NULL;
$arguments102['dir'] = NULL;
$arguments102['id'] = NULL;
$arguments102['lang'] = NULL;
$arguments102['style'] = NULL;
$arguments102['title'] = NULL;
$arguments102['accesskey'] = NULL;
$arguments102['tabindex'] = NULL;
$arguments102['onclick'] = NULL;
$renderChildrenClosure103 = function() use ($renderingContext, $self) {
return '
								Login
							';
};
$viewHelper104 = $self->getViewHelper('$viewHelper104', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper');
$viewHelper104->setArguments($arguments102);
$viewHelper104->setRenderingContext($renderingContext);
$viewHelper104->setRenderChildrenClosure($renderChildrenClosure103);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper

$output91 .= $viewHelper104->initializeArgumentsAndRender();

$output91 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments105 = array();
$arguments105['as'] = 'flashMessages';
$arguments105['additionalAttributes'] = NULL;
$arguments105['severity'] = NULL;
$arguments105['class'] = NULL;
$arguments105['dir'] = NULL;
$arguments105['id'] = NULL;
$arguments105['lang'] = NULL;
$arguments105['style'] = NULL;
$arguments105['title'] = NULL;
$arguments105['accesskey'] = NULL;
$arguments105['tabindex'] = NULL;
$arguments105['onclick'] = NULL;
$renderChildrenClosure106 = function() use ($renderingContext, $self) {
$output107 = '';

$output107 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments108 = array();
$arguments108['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessages', $renderingContext);
$arguments108['as'] = 'flashMessage';
$arguments108['key'] = '';
$arguments108['reverse'] = false;
$arguments108['iteration'] = NULL;
$renderChildrenClosure109 = function() use ($renderingContext, $self) {
$output110 = '';

$output110 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments111 = array();
// Rendering Boolean node
// Rendering Array
$array112 = array();
$array112['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array113 = array();
$array113['0'] = 'OK';
$arguments111['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array112, $array113);
$arguments111['then'] = NULL;
$arguments111['else'] = NULL;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-success">
									';
};
$viewHelper115 = $self->getViewHelper('$viewHelper115', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper115->setArguments($arguments111);
$viewHelper115->setRenderingContext($renderingContext);
$viewHelper115->setRenderChildrenClosure($renderChildrenClosure114);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output110 .= $viewHelper115->initializeArgumentsAndRender();

$output110 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments116 = array();
// Rendering Boolean node
// Rendering Array
$array117 = array();
$array117['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array118 = array();
$array118['0'] = 'Notice';
$arguments116['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array117, $array118);
$arguments116['then'] = NULL;
$arguments116['else'] = NULL;
$renderChildrenClosure119 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-notice">
									';
};
$viewHelper120 = $self->getViewHelper('$viewHelper120', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper120->setArguments($arguments116);
$viewHelper120->setRenderingContext($renderingContext);
$viewHelper120->setRenderChildrenClosure($renderChildrenClosure119);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output110 .= $viewHelper120->initializeArgumentsAndRender();

$output110 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments121 = array();
// Rendering Boolean node
// Rendering Array
$array122 = array();
$array122['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array123 = array();
$array123['0'] = 'Warning';
$arguments121['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array122, $array123);
$arguments121['then'] = NULL;
$arguments121['else'] = NULL;
$renderChildrenClosure124 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-warning">
									';
};
$viewHelper125 = $self->getViewHelper('$viewHelper125', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper125->setArguments($arguments121);
$viewHelper125->setRenderingContext($renderingContext);
$viewHelper125->setRenderChildrenClosure($renderChildrenClosure124);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output110 .= $viewHelper125->initializeArgumentsAndRender();

$output110 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments126 = array();
// Rendering Boolean node
// Rendering Array
$array127 = array();
$array127['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext);
// Rendering Array
$array128 = array();
$array128['0'] = 'Error';
$arguments126['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $array127, $array128);
$arguments126['then'] = NULL;
$arguments126['else'] = NULL;
$renderChildrenClosure129 = function() use ($renderingContext, $self) {
return '
										<div class="neos-tooltip neos-bottom neos-in neos-tooltip-error">
									';
};
$viewHelper130 = $self->getViewHelper('$viewHelper130', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper130->setArguments($arguments126);
$viewHelper130->setRenderingContext($renderingContext);
$viewHelper130->setRenderChildrenClosure($renderChildrenClosure129);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output110 .= $viewHelper130->initializeArgumentsAndRender();

$output110 .= '
										<div class="neos-tooltip-arrow"></div>
										<div class="neos-tooltip-inner">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments131 = array();
$arguments131['value'] = NULL;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.message', $renderingContext);
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper133->setArguments($arguments131);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure132);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output110 .= $viewHelper133->initializeArgumentsAndRender();

$output110 .= '</div>
									</div>
								';
return $output110;
};

$output107 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments108, $renderChildrenClosure109, $renderingContext);

$output107 .= '
							';
return $output107;
};
$viewHelper134 = $self->getViewHelper('$viewHelper134', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper134->setArguments($arguments105);
$viewHelper134->setRenderingContext($renderingContext);
$viewHelper134->setRenderChildrenClosure($renderChildrenClosure106);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper

$output91 .= $viewHelper134->initializeArgumentsAndRender();

$output91 .= '
						</div>
					</fieldset>
				';
return $output91;
};
$viewHelper135 = $self->getViewHelper('$viewHelper135', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper135->setArguments($arguments89);
$viewHelper135->setRenderingContext($renderingContext);
$viewHelper135->setRenderChildrenClosure($renderChildrenClosure90);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output85 .= $viewHelper135->initializeArgumentsAndRender();

$output85 .= '
			</div>
		</div>
		<div id="neos-login-footer">
			<p>
				<a href="http://neos.typo3.org" target="_blank">TYPO3 Neos</a> – © 2006-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments136 = array();
$arguments136['format'] = 'Y';
$arguments136['date'] = 'now';
$arguments136['forceLocale'] = NULL;
$arguments136['localeFormatType'] = NULL;
$arguments136['localeFormatLength'] = NULL;
$renderChildrenClosure137 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper138 = $self->getViewHelper('$viewHelper138', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper138->setArguments($arguments136);
$viewHelper138->setRenderingContext($renderingContext);
$viewHelper138->setRenderChildrenClosure($renderChildrenClosure137);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper

$output85 .= $viewHelper138->initializeArgumentsAndRender();

$output85 .= '
				This is free software, licensed under GPL3 or higher, and you are welcome to redistribute it under certain conditions; <a href="http://typo3.org/licenses" target="_blank">click for details.</a>
				TYPO3 Neos comes with ABSOLUTELY NO WARRANTY; <a href="http://typo3.org/licenses" target="_blank">click for details.</a>
				See <a href="http://neos.typo3.org" target="_blank">neos.typo3.org</a> for more details.
				Obstructing the appearance of this notice is prohibited by law.
			</p>
		</div>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments139 = array();
$arguments139['path'] = '2/js/bootstrap.min.js';
$arguments139['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments139['resource'] = NULL;
$arguments139['localize'] = true;
$renderChildrenClosure140 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper141 = $self->getViewHelper('$viewHelper141', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper141->setArguments($arguments139);
$viewHelper141->setRenderingContext($renderingContext);
$viewHelper141->setRenderChildrenClosure($renderChildrenClosure140);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output85 .= $viewHelper141->initializeArgumentsAndRender();

$output85 .= '"></script>
		<script>
			if ($(\'#username\').val()) {
				$(\'#password\').focus();
			}
			try {
				$(\'form[name="login"] input[name="lastVisitedNode"]\').val(sessionStorage.getItem(\'TYPO3.Neos.lastVisitedNode\'));
			} catch(e) {}
		</script>
	</body>
';
return $output85;
};

$output67 .= '';

$output67 .= '
';

return $output67;
}


}
#0             44189     